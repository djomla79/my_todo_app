package com.todo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.todo.entity.ToDo;

@Repository
public interface ToDoRepository extends MongoRepository<ToDo, ObjectId> {
	
	ToDo findOneById(String id);
	List<ToDo> findByActiveTrue();
	List<ToDo> findByActiveFalse();
	
}
