package com.todo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todo.entity.ToDo;
import com.todo.repository.ToDoRepository;

@RestController
@RequestMapping("/todo")
@CrossOrigin("http://localhost:3000")
public class ToDoController {
	
	private final ToDoRepository toDoRepository;
	
	@Autowired
	ToDoController(ToDoRepository toDoRepository) {
		this.toDoRepository = toDoRepository;
	}
	
	@PostMapping(value = "/save")
    public ToDo createToDo() {
		ToDo newtodo = new ToDo("fourth", "this is fourth todo", true);
        return toDoRepository.save(newtodo);
    }
	
	@GetMapping(value = "/all-active")
    public List<ToDo> getAllActive() {
        return toDoRepository.findByActiveTrue();
    }
	
	@GetMapping(value = "/all-inactive")
    public List<ToDo> getAllInactive() {
        return toDoRepository.findByActiveFalse();
    }
	
}